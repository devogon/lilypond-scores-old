\version "2.19.36"
\language "nederlands"

\header {
  title = "Pink Panther Theme"
  instrument = "Cello (2)"
  composer = "Henri Mancini"
  arranger = "Marcel den Os"
%  meter = "4/4"
%  piece = "Cello 2"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \major
  \time 4/4
  \tempo 4=116
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-letters
}

cello = \relative c {
  \global
  % Music follows here.
  R1*4/4*3

  r2 r8. cis16\mp^\markup{\italic "pizz."} (d8.\staccato ) dis16 (
  e4\staccato) r4 r2
  r2 r8. cis16 (d8.\staccato ) dis16 (
  e4\staccato) r4 r2
  r2 r8. cis16 (d8.\staccato ) dis16 (
  \bar "||"
  e4\staccato\segno)\mark \default r4 r2

    r2 r8. es16 (d8.\staccato ) dis16 (
  c4\staccato) r4 r2
    r2 r8. cis16 (d8.\staccato ) dis16 (
  e4\staccato) r4 r2
    r2 r8. d16 (dis8.\staccato ) e16 (
  f!4\staccato) r4 r2
    r2 r8. cis16 (d8.\staccato ) dis16 (
  e4\staccato) r4 r2
    r2 r8. es16 (d8.\staccato ) des16 (
  c4\staccato) r4 r2
    r2 r8. cis16 (d8.\staccato ) dis16 (
  e4\staccato) r4 r2
  c4 r b r
  e4 r b r
  e4^\markup{"to Coda"} r r2
  \bar "||"
  \mark \default
  \key bes \major
  fis,4\f^\markup{"arco"} g bes d
  fis4 g g, d'
  es4 g, bes des
  es4 bes' g es8. bes16
  g4 a bes c
  d4 g, bes8. d16 g,4 | % 30
  as4 as' ges es
  c4 as8. c16 es4 as,
  g4 a bes d
  g bes d bes8. g16
  es4 g bes bes,
  es4 bes' g es
  g4 g,8. bes16 c4 bes
  es4 bes' a d,8. d16
  g,4 a bes c
  d4 e! fis g
  \bar "||"
  \mark \default
  \key g \major
  e4^\markup{"Solo"} b\mp e fis
  % \break
  g4 a b g8. e16 | % 42
  c4 d e g, |
  c g' e c
  e fis g a
  b g b, d
  f!4 c a c
  % \break
  f,!4 g a c | % 48
  e4 b cis dis
  e fis g b
  c g e g8. e16
  c4 g' e c
  e fis g b,
  % \break
  c4 g' b, dis | % 54
  e4 fis g b,
  e4 b e f!
  \mark \default
  \bar "||"
  e4 b e fis
  g e g b
  c g e g8. g,16
  % \break
  c4 g c g' | % 60
  e4 g, a b
  e fis g e
  f!4 f, a c
  f!4 c a f'
  e4 b' g b,
  % \break
  e4 g, b e | % 66
  c4 d e g,
  c4 c' g c,
  e4 fis g b,
  c g' fis b,
  e4 g, a ais
  % \break
  b4 b' r8. cis,16^\markup{\italic "pizz."} (d8.\staccato) dis16 (
  \bar "||"
  \mark \default
  e4\staccato) r4 b r
  e,4 r r8. cis'16 (d8.\staccato) dis16 (
  e4\staccato) r b r
  e,4 r r8. cis'16^\markup{\bold \center-column {"D.S. al Coda"}} (d8.\staccato) dis16
  \break
  \mark \default
%  \once \override Score.RehearsalMark #'font-size = #8
  e8\mark \markup{\musicglyph #"scripts.coda"} e4.\ff e4 e
  f!4 f, f8 r f e\fermata (
  e1) (
  e8) r r4 r2
  \bar "|."
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}


\markup {
  \teeny
  \date
}