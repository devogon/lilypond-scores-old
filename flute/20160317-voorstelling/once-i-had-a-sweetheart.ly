\version "2.19.36"
\language "nederlands"


\header {
  title = "Uma"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  print-all-headers = ##t
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  \tempo "langzaam" 4=80
}

globalB = {
  \key c \major
  \numericTimeSignature
  \time 4/4
  \tempo "niet zo snel" 4=80
}
scoreAFlute = \relative c'' {
  \global
  % Music follows here.
  \partial 4 r4
    g2.^\markup{\bold "G,"}_\markup{"1, 2, 3"}
    f2.^\markup{\bold "F"}
    c'2.^\markup{\bold "C"}
    g2.^\markup{\bold "G"}
    g2.^\markup{\bold "G"}
    a2.^\markup{\bold "A"}
    a2.^\markup{\bold "A"}
    g2.^\markup{\bold "G"}
    \break
    g2.^\markup{\bold "G"}
    c2.^\markup{\bold "C"}
    f,2.^\markup{\bold "F"}
    g2.^\markup{\bold "G"}
    g2.^\markup{\bold "G"}
    g2.^\markup{\bold "G"}
    c2.^\markup{\bold "C"}
    g2.^\markup{\bold "G"}
    \bar "|."
}

\score {
  \new Staff \with {
				%instrum
    entName = "Flute"
    %shortInstrumentName = "Flute"
    midiInstrument = "flute"
  } \scoreAFlute
  \header { title = \markup{\circle{"28"} "Once I Had A Sweetheart"} }
  \layout { }
  \midi { }
}

scoreBFlute = \relative c'' {
  \globalB

  % Music follows here.
  \partial 4 r4
    a2^\markup{\bold "A"}_\markup{"1, 2"} a^\markup{\bold "A"}
    c^\markup{\bold "C"} g^\markup{\bold "G"}
    a2^\markup{\bold "A"} a^\markup{\bold "A"} 
    g1^\markup{\bold "G"}_\markup{\bold "1,2,3,4"}
    a2^\markup{\bold "A"} a^\markup{\bold "A"}
    c^\markup{\bold "C"} g^\markup{\bold "G"}
    a^\markup{\bold "A"} a^\markup{\bold "A"}
    a1^\markup{\bold "A"}
    \break
    c2^\markup{\bold "C"} a^\markup{\bold "A"}
    a^\markup{\bold "A"} c^\markup{\bold "C"}
    a^\markup{\bold "A"} a^\markup{\bold "A"}
    g1^\markup{\bold "G"}
    a2^\markup{\bold "A"} a^\markup{\bold "A"}
    c^\markup{\bold "C"} g^\markup{\bold "G"}
    c^\markup{\bold "C"} a^\markup{\bold "A"}
    a2.^\markup{\bold "A"}
    \bar "|."
}

\score {
  \new Staff \with {
    % instrumentName = "Flute"
    % shortInstrumentName = "Flute"
    midiInstrument = "flute"
  } \scoreBFlute
  \header { title = \markup{\circle{"7"} "Kingsfold"} }
  \layout { }
  \midi { }
}

\markup{\large "C = + wijsvinger"}