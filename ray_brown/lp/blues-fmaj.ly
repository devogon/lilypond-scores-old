\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues (I-III)"
  subtitle = "in f major"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
%  \tempo 4=100
}

contrabassI = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  f4 c a f |
  bes4 f' bes, c |
  f4 f es es |
  d4 d c c |
  \break
  bes8. bes'16 d,4 g f |
  bes4 c d e |
  f8.^\markup{"f"} f16 e4 d c8. a16 |
  d4 a a, d
  \break
  g4 d' bes g |
  c4 c, d e |
  f4 bes a d, |
  g4 f e c |
  \bar "||"
}
contrabassII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  f4 g a fis |
  g4 a bes gis |
  a4 bes c d |
  es4 g^\markup{"g"} f8.^\markup{"f"} f,16 f,4 |
  \break
  bes4 as'4 g ges |
  f4 d b! as |
  a4 d g \times 2/3 {c8 ces bes} |
  a4 es d fis, |
  \break
  g4 g' a a, |
  bes4 bes' b! b,! |
  c4 bes' a as |
  g4 des c e, |
}

contrabassIII = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  f,4 a fis a |
  g4 bes gis b! |
  a4 c b! d |
  c4 es f b,! |
  \break
  bes4 c d f |
  b,!4 c d e |
  f4 c ces e |
  es4 bes a d |
  \break
  bes'4 d g, f |
  e4 g c, bes |
  a4 g''^\markup{"g"} ges^\markup{"ges"} d, |
  des4 f'^\markup{"f"} e c, |
  \bar "||"
}


\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassI }
  \layout { }
  %\midi { }
  \header {
    piece = \markup{\bold "Blues (I)"}
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassII }
  \layout { }
    \header {
    piece = \markup{\bold "Blues (II)"}
  }
 % \midi { }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabassIII }
  \layout { }
 % \midi { }
   \header {
    piece = \markup{\bold "Blues (III)"}
  }
}
