\version "2.19.36"
\language "nederlands"

\header {
  title = "Blues"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent=0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
%  \tempo 4=100
}

contrabass = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  \bar "[|:"
  \mark \default
  \repeat volta 2 {
    bes4\mark \default r f' r
    bes,4 r f' r
    bes,4 r f' r
    bes,4 r f' bes,
    ees4 r bes r
  \break
    ees4 r c f
    bes,4 r d r
    g4 r d g
    c,4 r c r
    f4 r c f
    \break
    bes,4 r g r
    c4 r f r
    \bar ":|]"
  }\mark \default
  bes,4 f' bes, f'
  bes,4 f' bes, f'
  bes,4 c d f
  \break
  bes4 as g f
  es4 bes es bes
  es4 g c, g'
  bes4 d, es e!
  f4 d g g
  \break
  c,4 g' es c
  f4 es d c
  bes4 d g g
  c,4 d es f
  \break
  \mark \default


}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
