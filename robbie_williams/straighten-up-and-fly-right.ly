\version "2.19.36"
\language "nederlands"

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Straighten Up and Fly Right"

  composer = "Nat King Cole and Irving Mills"
  % Remove default LilyPond tagline
  % tagline = \markup {
  %   \tiny Engraved on
  %   \simple #(strftime "%d-%m-%Y at %X" (localtime (current-time)))
				% }
  tagline = "..."
}
				% medium swing
				% 1/4 = 144
				% 2 flats
				% words and music by nat king cole and krving mills
				% ©1944AmericanAcademyofMusicInc,USA
				% WarnerChappellMusicLtd.LondonW68B5
				% AVocalChordsSheetMusic™Edition


\paper {
  #(set-paper-size "a4")

  ragged-last-bottom = ##f
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
  \tempo 4=144
}

contrabass = \relative c, {
  \global
  \override Score.BarNumber.break-visibility = ##(#t #t #t)

  % Music follows here.
  \partial 8 r8
  bes2 d |
  es4. e!8 ~ e4 f |
  bes,2 d |
  es4. e!8 ~ e4 f |
  bes,2 d |
  es4. e!8 ~ e4 f |
  f8 f,-> ~ f4 f'8 f,-> ~ f4 |
  f'8 f,-> ~ f4 f'8 f,-> ~ f4 |
  bes4 r4 r2 | 
  es4 r4 r8 e!4. |
  f4 r r2 |
  r8 es r4 r8 a,4. |
  bes4 r4 r2 |
  es4 r4 r8 e!4. |
  f4 r r2 |
  r1 | % 16
  bes2 as |
  g2 f |
  es2 d |
  c2 f4 a |
  bes2 as |
  g2 f |
  e2 d |
  c2 f4 a |
  bes2 as |
  g2 f4 e! |
  es2 d |  % 27
  c2 f4 a | % 28
  bes2 as |
  g2 f |
  es2 d |
  c4 f bes bes, |
  d4 fis g gis |
  a4 a, d a' |
  g8. a16 a,4 c cis |
  d4 d g b! |
  c4 g e! d | % 37
  c4 cis d e! |
  f4 r r2 |
  r1 |
  bes2 as |
  g2 f |
  es2 d |
  c2 f4 a |
  bes2 as |
  g2 f |
  es d |
  c4 f bes8 as a! bes ~ | % 48
  bes8 bes bes,4 g' f |
  es4 es e! f |
  bes4 d, g f |
  es4 es e! f |
  bes,4 bes' as as |
  g4 g f f |
  es4 es d d | % 55
  c4 c f f, |
  bes4 ces c! d |
  es4 es e! f |
  bes,4 d g f |
  es4 es e! f |
  bes,4 bes' as as |
  g g f f |
  es4 es d d | % 63
  c4 f bes, f' |
  bes,2 d |
  es4. e!8 ~ e4 f |
  bes,2 d |
  es4. e!8 ~ e4 f |
  bes,2 d |
  es4. e!8 ~ e4 f |
  f8 f,-> ~ f4 f'8 f,-> ~ f4 |
  f'8 f,-> ~ f4 f'8 f,-> ~ f4 | % 72
  bes'2 as |
  g2 f |
  es2 d |
  c2 f4 a |
  bes2 as |
  g2 f |

  es2 d |
  c2 f4 a |
  bes2 as |
  g2 f4 e! | % 82
  es2 d |
  c2 f4 a |
  bes2 as |
  g2 f4. es8 |
  r8 es r4 r2 |
  r8 c4. r8 f4. |
  r8 bes a4\staccato g\staccato ges\staccato |
  f4\staccato r r8 bes,8.\staccato-> r4
  \bar "|."
}

\score {
  \new Staff { \clef "bass_8" \contrabass }
  \layout { }
}
\markup {
  \teeny
  \date
}


% A buzzard took a monkey for a ride in the air,
% The monkey thought that everything
% was on the square.
% The buzzard tried to throw the monkey
% off his back,
% The monkey grabbed his neck and said,
% "Now listen, Jack..."

% Straighten up and fly right,
% Straighten up and fly right
% Straighten up and fly right
% Cool down, papa, don't you blow your top.
% Ain't no use in divin',
% What's the use in jivin'?
% Straighten up and fly right
% Cool down, papa, don't you blow your top.

% The buzzard told the monkey,
% You're chokin' me.
% Release your hold and I'll set you free.
% The monkey looked the buzzard right
% dead in the eye and said,
% Your story's so touching, but it sounds
% jes' like a lie.

% Straighten up and fly right
% Straighten up and stay right
% Straighten up and fly right
% Cool down, papa, don't you blow your top.

% Straighten up and fly right
% Straighten up and stay right
% Straighten up and fly right
% Cool down, papa, don't you blow - your - top.

% Fly right! 